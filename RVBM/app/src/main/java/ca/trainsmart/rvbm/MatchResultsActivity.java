package ca.trainsmart.rvbm;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MatchResultsActivity extends AppCompatActivity {

    private static final String TAG = "State";

    private RecyclerView recyclerView;
    private DatabaseReference dbRef;
    private FirebaseAuth mAuth;
    private ArrayList<String> lstName, lstProfilePic, lstAccountType, lstUID;
    private MatchAdapter matchAdapter;
    private String preferredGender, selectedProf;
    private boolean pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_results);

        recyclerView = findViewById(R.id.recycleResultView);
        lstName = new ArrayList<>();
        lstAccountType = new ArrayList<>();
        lstProfilePic = new ArrayList<>();
        lstUID = new ArrayList<>();

        mAuth = FirebaseAuth.getInstance();
        dbRef = FirebaseDatabase.getInstance().getReference();

        Intent intent = getIntent();
        preferredGender = intent.getStringExtra("genderPref");
        selectedProf = intent.getStringExtra("selectedProf");

        matchAdapter = new MatchAdapter(MatchResultsActivity.this, lstName, lstProfilePic, lstAccountType, lstUID);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(MatchResultsActivity.this));
        recyclerView.addItemDecoration(new DividerItemDecoration(MatchResultsActivity.this, LinearLayout.VERTICAL));
        recyclerView.setAdapter(matchAdapter);
        setAdapter();
        matchAdapter.notifyDataSetChanged();
    }


    private void setAdapter() {
        dbRef.child("user").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                //Clear lists before searching
                lstName.clear();
                lstProfilePic.clear();
                lstAccountType.clear();
                lstUID.clear();
                recyclerView.removeAllViews();
                Log.d("lists", "Names: " + lstName.size() + " ProfilePics: " + lstProfilePic.size() + " Types: " + lstAccountType.size() + " UIDS: " + lstUID.size());

                int itemCount = 0;

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Log.d("Snapshot", "Snapshot: " + snapshot);
                    String uid = snapshot.getKey();
                    Log.d("UID", "uid: " + uid);

                    String name, profilePicUrl, gender, accountType, profession;
                    if (uid != null) {
                        try {
                            name = snapshot.child("name").getValue(String.class);
                            profilePicUrl = snapshot.child("imgURL").getValue(String.class);
                            gender = snapshot.child("gender").getValue(String.class);
                            accountType = snapshot.child("accountType").getValue(String.class);
                            profession = snapshot.child("profession").getValue(String.class);
                            Log.d("userName", "userName: " + name);
                            Log.d("gender", "gender: " + gender);
                            Log.d("profession", "profession: " + profession);
                            /*Log.i(TAG,"MatchResultsActivity: " + selectedProf
                                + "/" + preferredGender);*/
                            try {
                                pref = preferredGender.equals("No preference");
                                //Log.i(TAG, String.valueOf(pref));
                                if (pref) {
                                    if (profession.toLowerCase().matches("(.*)" + selectedProf.toLowerCase() + "(.*)")) {
                                        lstName.add(name);
                                        lstProfilePic.add(profilePicUrl);
                                        lstAccountType.add(profession);
                                        lstUID.add(uid);
                                        Log.d("ProfName", "Name: " + lstName.get(itemCount));
                                        Log.d("lists", "Names: " + lstName.size() + " ProfilePics: " + lstProfilePic.size() + " Types: " + lstAccountType.size() + " UIDS: " + lstUID.size());
                                        itemCount++;
                                    }
                                } else {
                                    Log.i(TAG, "else clause");
                                    if (profession.toLowerCase().matches("(.*)" + selectedProf.toLowerCase() + "(.*)") && gender.toLowerCase().equals(preferredGender.toLowerCase())) {
                                        lstName.add(name);
                                        lstProfilePic.add(profilePicUrl);
                                        lstAccountType.add(accountType);
                                        lstUID.add(uid);
                                        Log.d("ProfName", "Name: " + lstName.get(itemCount));
                                        Log.d("lists", "Names: " + lstName.size() + " ProfilePics: " + lstProfilePic.size() + " Types: " + lstAccountType.size() + " UIDS: " + lstUID.size());
                                        itemCount++;
                                    }
                                }
                            } catch (Exception e) {
                                //Toast.makeText(MatchResultsActivity.this, "Info unavailable", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e ) {
                            Log.d("ErrorGet", "Error getting child values");
                            itemCount++;
                        }
                        if (itemCount == 15) {
                            break;
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
