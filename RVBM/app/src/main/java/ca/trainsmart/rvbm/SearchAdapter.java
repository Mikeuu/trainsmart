package ca.trainsmart.rvbm;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> {
    Context context;
    private ArrayList<String> lstName, lstProfilePic, lstProfession, lstUID;

    class SearchViewHolder extends RecyclerView.ViewHolder {
        ImageView profilePicture;
        TextView listName, listProfession;

        public SearchViewHolder(View itemView) {
            super(itemView);
            profilePicture = (ImageView) itemView.findViewById(R.id.listImg);
            listName = (TextView) itemView.findViewById(R.id.listName);
            listProfession = (TextView) itemView.findViewById(R.id.userProfession);
        }
    }

    public SearchAdapter(Context context, ArrayList<String> lstName, ArrayList<String> lstProfilePic, ArrayList<String> lstProfession, ArrayList<String> lstUID) {
        this.context = context;
        this.lstName = lstName;
        this.lstProfilePic = lstProfilePic;
        this.lstProfession = lstProfession;
        this.lstUID = lstUID;
    }

    @NonNull
    @Override
    public SearchAdapter.SearchViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_layout, viewGroup, false);

        return new SearchAdapter.SearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SearchViewHolder searchViewHolder,  int i) {
        final int uidPos = i;
        searchViewHolder.listName.setText(lstName.get(i));
        searchViewHolder.listProfession.setText(lstProfession.get(i));
        Glide.with(context)
                .load(lstProfilePic.get(i))
                .into(searchViewHolder.profilePicture);

        searchViewHolder.listProfession.setText(lstProfession.get(i));

        searchViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Item was clicked: UID: " + lstUID.get(uidPos), Toast.LENGTH_SHORT).show();
                context.startActivity(new Intent(context, ViewOtherProfile.class)
                .putExtra("UID", lstUID.get(uidPos)));
            }
        });
    }

    @Override
    public int getItemCount() {
        return lstName.size();
    }
}
