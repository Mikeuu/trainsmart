package ca.trainsmart.rvbm;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

public class ConversationAdapter extends RecyclerView.Adapter<ConversationAdapter.ConversationViewHolder> {
    Context context;
    private List<User> lstUsers;

    class ConversationViewHolder extends RecyclerView.ViewHolder {
        TextView usersname, profession;
        ImageView profilePic;

        public ConversationViewHolder(View itemView) {
            super(itemView);
            usersname = itemView.findViewById(R.id.listName);
            profession = itemView.findViewById(R.id.lstProfession);
            profilePic = itemView.findViewById(R.id.listImg);
        }
    }

    public ConversationAdapter(Context context, List<User> lstUsers) {
        this.context = context;
        this.lstUsers = lstUsers;

    }

    @NonNull
    @Override
    public ConversationAdapter.ConversationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_message, viewGroup, false);

        return new ConversationAdapter.ConversationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ConversationViewHolder viewHolder,  int i) {
        Log.d("Users", lstUsers.toString());
        User user = lstUsers.get(i);
        final String recipientUID = user.getUid();
        Log.d("recipient", recipientUID);
        viewHolder.usersname.setText(user.getName());
        viewHolder.profession.setText(user.getProfession());
        try {
            Glide.with(context)
                    .load(user.getImgURL())
                    .into(viewHolder.profilePic);
        } catch (Exception e) {
            Log.d("Glide", "Glide error");
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity( new Intent(context, MessageActivity.class)
                        .putExtra("recipientUID", recipientUID));
            }
        });

    }

    @Override
    public int getItemCount() {
        return lstUsers.size();
    }
}