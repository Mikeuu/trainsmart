package ca.trainsmart.rvbm;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ImageButton btnProfile, btnMessages;
    private Button btnMatch;
    private EditText searchText;
    private DatabaseReference dbRef, likesRef;
    private FirebaseUser fbUser;
    private FirebaseAuth mAuth;
    private ArrayList<String> lstName, lstProfilePic, lstUID, lstCity, lstProfession;
    private SearchAdapter searchAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        recyclerView = (RecyclerView) findViewById(R.id.RecyclerView1);
        btnProfile = (ImageButton) findViewById(R.id.btnProfile);
        btnMatch = (Button) findViewById(R.id.btnMatch);
        btnMessages = (ImageButton) findViewById(R.id.btnMessage);
        searchText = (EditText) findViewById(R.id.txtSearch);

        mAuth = FirebaseAuth.getInstance();
        dbRef = FirebaseDatabase.getInstance().getReference();
        likesRef = FirebaseDatabase.getInstance().getReference().child("Honours");
        fbUser = mAuth.getCurrentUser();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));

        lstName = new ArrayList<>();
        lstProfilePic = new ArrayList<>();
        lstUID = new ArrayList<>();
        lstCity = new ArrayList<>();
        lstProfession = new ArrayList<>();


        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().isEmpty()) {
                    setAdapter(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(HomeActivity.this, ViewProfileActivity.class
                ));
            }
        });

        btnMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, MatchActivity.class));
            }
        });

        btnMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, ViewConversations.class));
            }
        });

    }

    private void setAdapter(final String searchString) {
        dbRef.child("user").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (searchString.isEmpty()) {
                    recyclerView.removeAllViews();
                } else {
                    //Clear lists before searching
                    lstName.clear();
                    lstProfilePic.clear();
                    lstProfession.clear();
                    lstCity.clear();
                    lstUID.clear();
                    recyclerView.removeAllViews();
                    recyclerView.setBackground(getDrawable(R.drawable.transparent));

                    int itemCount = 0;

                    for (final DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Log.d("Snapshot", "Snapshot: " + snapshot);
                        final String uid = snapshot.getKey();
                        Log.d("UID", "uid: " + uid);

                        if (uid != null) {
                            String name = snapshot.child("name").getValue(String.class);
                            String profilePicUrl = snapshot.child("imgURL").getValue(String.class);
                            String profession = snapshot.child("profession").getValue(String.class);
                            String city = snapshot.child("city").getValue(String.class);

                            Log.d("userName", "userName: " + name);
                            String accountType = snapshot.child("accountType").getValue(String.class);

                            try {
                                if (accountType.contains("Health Professional") && name.toLowerCase().contains(searchString.toLowerCase())) {
                                    lstName.add(name);
                                    lstProfilePic.add(profilePicUrl);
                                    lstProfession.add(profession);
                                    lstCity.add(city);
                                    lstUID.add(uid);
                                    itemCount++;

                                    searchAdapter = new SearchAdapter(HomeActivity.this, lstName, lstProfilePic, lstProfession, lstUID);
                                    recyclerView.setAdapter(searchAdapter);
                                }
                            } catch (Exception e) {
                                Toast.makeText(HomeActivity.this, "Info unavailable", Toast.LENGTH_SHORT).show();
                            }

                            if (itemCount == 15) {
                                break;
                            }


                        }
                    }


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
}
