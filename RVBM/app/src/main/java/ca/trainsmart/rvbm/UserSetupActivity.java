package ca.trainsmart.rvbm;

import android.app.DatePickerDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.util.Calendar;

public class UserSetupActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private EditText txtName, txtEmail, txtCity, dtBday, txtAboutMe;
    private LinearLayout profLayout, patientLayout;
    private Switch userType;
    private Button btnFinish;
    private String uid, accountType, city, bday, email, name, aboutMe, profession, gender;
    private int userTypeInt = 1;
    private Spinner spinnerPro, spinnerGen;
    private String[] professionList = {"Kinesiology", "Physiotherapy", "Chiropractor", "Massage Therapist ", "Dietitian", "Naturopathy"};
    private String[] genderList = {"Male", "Female"};
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private int likes = 0, dislikes = 0, hcommunication = 0, hInformative = 0, hProfessionalism = 0;

    DatabaseReference databaseUserRef;
    FirebaseAuth mAuth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_setup);

        txtName = (EditText) findViewById(R.id.txtNameSetup);
        txtEmail = (EditText) findViewById(R.id.txtEmailSetup);
        txtAboutMe = (EditText) findViewById(R.id.txtAboutMe);
        txtCity = (EditText) findViewById(R.id.txtCity);
        dtBday = (EditText) findViewById(R.id.dtBirthday);
        userType = (Switch) findViewById(R.id.swUserType);
        profLayout = (LinearLayout) findViewById(R.id.llProf);
        patientLayout = (LinearLayout) findViewById(R.id.llPatient);
        btnFinish = (Button) findViewById(R.id.btnSubmit);
        spinnerPro = (Spinner) findViewById(R.id.sProfession);
        spinnerGen = (Spinner) findViewById(R.id.sGender);
        ArrayAdapter<String> adapterPro = new ArrayAdapter<String>(UserSetupActivity.this, android.R.layout.simple_spinner_item, professionList);
        ArrayAdapter<String> adapterGen = new ArrayAdapter<String>(UserSetupActivity.this, android.R.layout.simple_spinner_item, genderList);
        spinnerPro.setAdapter(adapterPro);
        spinnerGen.setAdapter(adapterGen);


        mAuth = FirebaseAuth.getInstance();
        uid = mAuth.getCurrentUser().getUid();
        databaseUserRef = FirebaseDatabase.getInstance().getReference();


        dtBday.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                DialogFragment datePicker = new ca.trainsmart.rvbm.DatePicker();
                datePicker.show(getSupportFragmentManager(), "Date Picker");
            }
        });

        userType.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    profLayout.setVisibility(View.VISIBLE);
                    patientLayout.setVisibility(View.GONE);
                    spinnerPro.setVisibility(View.VISIBLE);
                } else {
                    profLayout.setVisibility(View.GONE);
                    patientLayout.setVisibility(View.VISIBLE);
                    spinnerPro.setVisibility(View.GONE);
                }
            }
        });

        Intent intent = getIntent();
        email = intent.getStringExtra("Email");
        txtEmail.setText(email);


        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                uid = mAuth.getCurrentUser().getUid();
                bday = dtBday.getText().toString().trim();
                city = txtCity.getText().toString().trim();
                name = txtName.getText().toString().trim();
                aboutMe = txtAboutMe.getText().toString().trim();
                gender = spinnerGen.getSelectedItem().toString();
                if(!TextUtils.isEmpty(name) || !TextUtils.isEmpty(city) || !TextUtils.isEmpty(bday) || !TextUtils.isEmpty(aboutMe)) {
                    if (userType.isChecked()) {
                        userTypeInt = 2;
                        accountType = "Health Professional";
                        Toast.makeText(UserSetupActivity.this, "usertypeint is 2", Toast.LENGTH_SHORT).show();
                    } else {
                        userTypeInt = 1;
                        accountType = "Patient";
                        Toast.makeText(UserSetupActivity.this, "usertypeint is 1", Toast.LENGTH_SHORT).show();
                    }

                    User userBase;

                    if (userTypeInt == 1) {
                        //User account
                        userBase = new User(uid, email, name, aboutMe, accountType, bday, city, gender);
                        profession = "Patient";
                    } else {
                        //Professional Account
                        profession = spinnerPro.getSelectedItem().toString();
                        userBase = new User(uid, email, name, aboutMe, accountType, bday, city, profession, gender);
                    }
                    databaseUserRef.child("user").child(uid).setValue(userBase);
                    Toast.makeText(UserSetupActivity.this, "Account info updated", Toast.LENGTH_SHORT).show();
                    UserSetupActivity.this.finish();
                    startActivity(new Intent(UserSetupActivity.this, ViewProfileActivity.class));
                } else {
                    txtName.setError("Please make sure all fields are not empty");
                    txtName.requestFocus();
                    return;
                }
            }
        });
    }

    @Override
    public void onDateSet(android.widget.DatePicker datePicker, int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        dob.set(Calendar.YEAR, year);
        dob.set(Calendar.MONTH, month);
        dob.set(Calendar.DAY_OF_MONTH, day);

        Calendar now = Calendar.getInstance();

        if (dob.after(now)) {
            throw new IllegalArgumentException("Can't be born in the future");
        }
        int year1 = now.get(Calendar.YEAR);
        int year2 = dob.get(Calendar.YEAR);
        int age = year1 - year2;
        int month1 = now.get(Calendar.MONTH);
        int month2 = dob.get(Calendar.MONTH);
        if (month2 > month1) {
            age--;
        } else if (month1 == month2) {
            int day1 = now.get(Calendar.DAY_OF_MONTH);
            int day2 = dob.get(Calendar.DAY_OF_MONTH);
            if (day2 > day1) {
                age--;
            }
        }

        String date = DateFormat.getDateInstance().format(dob.getTime());
        dtBday.setText(Integer.toString(age));
    }
}

