package ca.trainsmart.rvbm;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;

public class ProfileActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    private static final String TAG = "State";

    private static final int PICK_PROFILE_PICTURE = 1111;

    private ImageView profilePic;
    private EditText txtName, txtDesc, txtDate, txtCity;
    private TextView txtProfession;
    private Uri uriProfilePicture;
    private FirebaseUser user;
    private FirebaseAuth mAuth;
    private DatabaseReference userInfoRef;
    private String currentUID;
    private boolean accountPlus;

    @Override
    public void onDateSet(android.widget.DatePicker datePicker, int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        dob.set(Calendar.YEAR, year);
        dob.set(Calendar.MONTH, month);
        dob.set(Calendar.DAY_OF_MONTH, day);

        Calendar now = Calendar.getInstance();

        if (dob.after(now)) {
            throw new IllegalArgumentException("Can't be born in the future");
        }
        int year1 = now.get(Calendar.YEAR);
        int year2 = dob.get(Calendar.YEAR);
        int age = year1 - year2;
        int month1 = now.get(Calendar.MONTH);
        int month2 = dob.get(Calendar.MONTH);
        if (month2 > month1) {
            age--;
        } else if (month1 == month2) {
            int day1 = now.get(Calendar.DAY_OF_MONTH);
            int day2 = dob.get(Calendar.DAY_OF_MONTH);
            if (day2 > day1) {
                age--;
            }
        }

        //String date = DateFormat.getDateInstance().format(dob.getTime());
        txtDate.setText(Integer.toString(age));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        currentUID = mAuth.getCurrentUser().getUid();
        userInfoRef = FirebaseDatabase.getInstance().getReference();

        profilePic = (ImageView) findViewById(R.id.listImg);
        txtName = (EditText) findViewById(R.id.txtName);
        txtDesc = (EditText) findViewById(R.id.txtDescription);
        txtDate = (EditText) findViewById(R.id.txtDate);
        txtCity = (EditText) findViewById(R.id.txtCityEditProfile);
        txtProfession = (TextView) findViewById(R.id.txtProfession);

        txtDate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                DialogFragment datePicker = new ca.trainsmart.rvbm.DatePicker();
                datePicker.show(getSupportFragmentManager(), "Date Picker");
            }
        });

        findViewById(R.id.btnSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveProfileInfo();
            }
        });
        //Select Profile picture
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImagePicker();
            }
        });

        userInfoRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                getData(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void showImagePicker() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Profile Picture"), PICK_PROFILE_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_PROFILE_PICTURE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            uriProfilePicture = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uriProfilePicture);
                profilePic.setImageBitmap(bitmap);
                uploadImageFirebase();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void getData(DataSnapshot dataSnapshot) {
        User user = new User();
        for(DataSnapshot ds : dataSnapshot.getChildren()) {
            Log.d("DataSnapshot", "DataSnapshot: " + ds);
            if(ds.getKey().equals("user")) {
                user.setName(ds.child(currentUID).getValue(User.class).getName());

                try {
                    user.setImgURL(ds.child(currentUID).getValue(User.class).getImgURL());
                } catch (Exception e) {
                    Log.d("ProfilePic", "User does not have a profil pic");
                }

                user.setName(ds.child(currentUID).getValue(User.class).getName());
                user.setBio(ds.child(currentUID).getValue(User.class).getBio());
                user.setAge(ds.child(currentUID).getValue(User.class).getAge());
                user.setCity(ds.child(currentUID).getValue(User.class).getCity());
                user.setAccountType(ds.child(currentUID).getValue(User.class).getAccountType());
                user.setGender(ds.child(currentUID).getValue(User.class).getGender());
                accountPlus = user.getAccountType().equals("Health Professional");
                if (accountPlus) {
                    user.setProfession(ds.child(currentUID).getValue(User.class).getProfession());
                    Log.i(TAG, user.getProfession());
                }
            }
        }
        txtName.setText(user.getName());
        txtDate.setText(user.getAge());
        txtDesc.setText(user.getBio());
        txtCity.setText(user.getCity());
        if (accountPlus) {
            txtProfession.setVisibility(View.VISIBLE);
            txtProfession.setText(user.getProfession());
        }
        Log.i(TAG, String.valueOf(accountPlus));

        try {
            Glide.with(ProfileActivity.this)
                    .load(user.getImgURL())
                    .into(profilePic);
        } catch(Exception e) {
            Log.d("ProfilePic", "User does not have a profil pic");
        }

    }


    //Upload to Firebase storage
    private void uploadImageFirebase() {
        final StorageReference profilePictureRef = FirebaseStorage.getInstance().getReference("img/userProfilePictures/" + currentUID + ".jpg");
        if (uriProfilePicture != null) {
            profilePictureRef.putFile(uriProfilePicture).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    //Get download url
                    final Task<Uri> downloadUrl = profilePictureRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            String uriString = uri.toString();
                            userInfoRef.child("user").child(currentUID).child("imgURL").setValue(uriString).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(ProfileActivity.this, "URL stored in database", Toast.LENGTH_SHORT).show();
                                    } else {
                                        String message = task.getException().getMessage();
                                        Toast.makeText(ProfileActivity.this, "URL storage failed: " + message, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    });
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(ProfileActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void saveProfileInfo() {
        String userName = txtName.getText().toString().trim();
        String userDesc = txtDesc.getText().toString().trim();
        String dateString = txtDate.getText().toString().trim();
        String city = txtCity.getText().toString().trim();
        if (userName.isEmpty()) {
            txtName.setError("Name required");
            txtName.requestFocus();
            return;
        } else {
            HashMap userMap = new HashMap();
            userMap.put("name", userName);
            userMap.put("bio", userDesc);
            userMap.put("age", dateString);
            userMap.put("city", city);
            userInfoRef.child("user").child(currentUID).updateChildren(userMap).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(ProfileActivity.this, "Account updated successfully", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}


