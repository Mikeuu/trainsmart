package ca.trainsmart.rvbm;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import timber.log.Timber;

import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ViewConversations extends AppCompatActivity {

    private RecyclerView ConvoRecyclerView;

    private ConversationAdapter convoAdapter;


    FirebaseUser mUser;
    DatabaseReference convoRef, usersRef, messageRef;
    private List<String> lastMessage;

    private List<User> users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_conversations);

        ConvoRecyclerView = (RecyclerView) findViewById(R.id.conversationRecyclerView);
        ConvoRecyclerView.setLayoutManager(new LinearLayoutManager(ViewConversations.this));
        ConvoRecyclerView.setHasFixedSize(true);

        mUser = FirebaseAuth.getInstance().getCurrentUser();

        convoRef = FirebaseDatabase.getInstance().getReference().child("Conversations").child(mUser.getUid());
        users = new ArrayList<>();
        convoRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User user = snapshot.getValue(User.class);
                    String uid = user.getUid();

                    Log.d("convoUID", uid);
                    if(uid != null) {
                        users.clear();
                        ConvoRecyclerView.setBackground(getDrawable(R.drawable.transparent));
                        findUsers(uid);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void findUsers(final String uid) {

        usersRef = FirebaseDatabase.getInstance().getReference().child("user");
        usersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User user = snapshot.getValue(User.class);
                    Log.d("testies", uid);
                    Log.d("testies2", user.getUid());
                    if(user.getUid().equals(uid)) {
                        Log.d("Users", user.getName());
                        users.add(user);
                        Log.d("UsersTest1", users.get(0).getUid());
                    }
                }

                convoAdapter = new ConversationAdapter(ViewConversations.this, users);
                ConvoRecyclerView.setAdapter(convoAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}