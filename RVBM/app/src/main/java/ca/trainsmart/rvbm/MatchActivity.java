package ca.trainsmart.rvbm;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class MatchActivity extends AppCompatActivity {

    private static final String TAG = "State";

    private String selectedProfession, preferredGender;
    private ScrollView kinView;
    private Spinner spnProf, spnGender;
    private Button btnSubmit;
    private DatabaseReference dbRef;
    private FirebaseUser fbUser;
    private FirebaseAuth mAuth;
    private ArrayList<String> lstName, lstProfilePic, lstAccountType, lstUID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match);

        spnProf = (Spinner) findViewById(R.id.spnProf);
        spnGender = (Spinner) findViewById(R.id.spnGender);
        kinView = (ScrollView) findViewById(R.id.kinView);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        mAuth = FirebaseAuth.getInstance();
        dbRef = FirebaseDatabase.getInstance().getReference();
        fbUser = mAuth.getCurrentUser();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findHealthPracticioner();
            }
        });

        spnProf.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //"Kinesiology", "Physiotherapy", "Chiropractor", "Massage Therapist ", "Dietitian", "Naturopathy"
                kinView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void findHealthPracticioner() {
        /*Log.i(TAG,"MatchActivity/findHealthPracticioner: " + selectedProfession +
                " Gender: " + spnGender.getSelectedItem());*/
        if(spnProf.getSelectedItem().toString().equals("Kinesiologist and Training")) {
            selectedProfession = "Kinesiology";

        } else if(spnProf.getSelectedItem().toString().equals("Physio")) {
            selectedProfession = "Physiotherapy";

        } else if(spnProf.getSelectedItem().toString().equals("Chiro")) {
            selectedProfession = "Chiropractor";

        } else if(spnProf.getSelectedItem().toString().equals("RMT")) {
            selectedProfession = "Massage Therapist";

        } else if(spnProf.getSelectedItem().toString().equals("Dietitian")) {
            selectedProfession = "Dietitian";

        } else if(spnProf.getSelectedItem().toString().equals("Naturopath")) {
            selectedProfession = "Naturopathy";

        }

        preferredGender = spnGender.getSelectedItem().toString();
        startActivity(new Intent(MatchActivity.this, MatchResultsActivity.class)
                .putExtra("genderPref", preferredGender)
                .putExtra("selectedProf", selectedProfession)
        );
    }


}
