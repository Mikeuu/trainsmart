package ca.trainsmart.rvbm;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ViewOtherProfile extends AppCompatActivity {

    private ImageView profilePic;
    private TextView lblName, lblDesc, lblDOB, lblCity, lblLikes, lblDislikes, lblComm, lblInfo, lblProf;
    private ImageButton btnLike, btnDislike, btnComm, btnInfo, btnProf, btnMessage, btnHome;

    private FirebaseAuth mAuth;
    private DatabaseReference userInfoRef, userLikesRef;
    private String userUID, recipientEmail;
    private Boolean userLiked, userDisliked, userComm, userInfo, userProf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_other_profile);

        profilePic = (ImageView) findViewById(R.id.ivProfilePic);
        lblName = (TextView) findViewById(R.id.lblName);
        lblDesc = (TextView) findViewById(R.id.lblDescription);
        lblDOB = (TextView) findViewById(R.id.lbDateOfBirth);
        lblCity = (TextView) findViewById(R.id.lbCity);
        lblLikes = (TextView) findViewById(R.id.txtLikes);
        lblDislikes = (TextView) findViewById(R.id.txtDislikes);
        lblComm = (TextView) findViewById(R.id.txtCommunication);
        lblInfo = (TextView) findViewById(R.id.txtInformative);
        lblProf = (TextView) findViewById(R.id.txtProfessionalism);
        btnMessage = (ImageButton) findViewById(R.id.btnMessage);
        btnHome = (ImageButton) findViewById(R.id.btnHome);
        btnLike = (ImageButton) findViewById(R.id.thumbUpDefault);
        btnDislike = (ImageButton) findViewById(R.id.thumbDownDefault);
        btnComm = (ImageButton) findViewById(R.id.Communication);
        btnInfo = (ImageButton) findViewById(R.id.Informative);
        btnProf = (ImageButton) findViewById(R.id.Professionalism);


        btnMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openChat();
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userLiked = true;
                userDisliked = false;
                updateLikes();
            }
        });
        btnDislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userLiked = false;
                userDisliked = true;
                updateLikes();
            }
        });
        btnComm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userComm = true;
                userProf = false;
                userInfo = false;
                updateHonours();
            }
        });
        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userComm = false;
                userProf = false;
                userInfo = true;
                updateHonours();
            }
        });
        btnProf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userComm = false;
                userProf = true;
                userInfo = false;
                updateHonours();
            }
        });


        mAuth  = FirebaseAuth.getInstance();
        userUID = getIntent().getStringExtra("UID");
        userInfoRef = FirebaseDatabase.getInstance().getReference();
        userLikesRef = FirebaseDatabase.getInstance().getReference().child("Honours");

        userInfoRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                getData(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        getLikes();

    }

    private void getLikes() {
        userLikesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("Likes").child(userUID).hasChild(mAuth.getCurrentUser().getUid())) {
                    btnLike.setBackground(getDrawable(R.drawable.thumb_up_red_24dp));
                    btnDislike.setBackground(getDrawable(R.drawable.thumb_down_black_24dp));
                } else if (dataSnapshot.child("Dislikes").child(userUID).hasChild(mAuth.getCurrentUser().getUid())) {
                    btnLike.setBackground(getDrawable(R.drawable.thumb_up_black_24dp));
                    btnDislike.setBackground(getDrawable(R.drawable.thumb_down_red_24dp));
                } else {
                    btnLike.setBackground(getDrawable(R.drawable.thumb_up_black_24dp));
                    btnDislike.setBackground(getDrawable(R.drawable.thumb_down_black_24dp));
                }

                if (dataSnapshot.child("Communication").child(userUID).hasChild(mAuth.getCurrentUser().getUid())) {
                    btnComm.setBackground(getDrawable(R.drawable.communcation_red_24dp));
                } else {
                    btnComm.setBackground(getDrawable(R.drawable.communication_black_24dp));
                }
                if (dataSnapshot.child("Informative").child(userUID).hasChild(mAuth.getCurrentUser().getUid())) {
                    btnInfo.setBackground(getDrawable(R.drawable.informative_red_24dp));
                } else {
                    btnInfo.setBackground(getDrawable(R.drawable.informative_black_24dp));
                }
                if (dataSnapshot.child("Professionalism").child(userUID).hasChild(mAuth.getCurrentUser().getUid())) {
                    btnProf.setBackground(getDrawable(R.drawable.professionalism_red_24dp));
                } else {
                    btnProf.setBackground(getDrawable(R.drawable.professionalism_black_24dp));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void updateHonours() {
        userLikesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(userComm) {
                    if(dataSnapshot.child("Communication").child(userUID).hasChild(mAuth.getCurrentUser().getUid())) {
                        //Remove if already exists
                        userLikesRef.child("Communication").child(userUID).child(mAuth.getCurrentUser().getUid()).removeValue();
                        btnComm.setBackground(getDrawable(R.drawable.communication_black_24dp));
                        userComm = false;
                    } else {
                        userLikesRef.child("Communication").child(userUID).child(mAuth.getCurrentUser().getUid()).setValue("Placeholder Value");
                        btnComm.setBackground(getDrawable(R.drawable.communcation_red_24dp));
                        userComm = false;
                    }
                } else if(userInfo) {
                    if(dataSnapshot.child("Informative").child(userUID).hasChild(mAuth.getCurrentUser().getUid())) {
                        //Remove if already exists
                        userLikesRef.child("Informative").child(userUID).child(mAuth.getCurrentUser().getUid()).removeValue();
                        btnInfo.setBackground(getDrawable(R.drawable.informative_black_24dp));
                        userInfo = false;
                    } else {
                        userLikesRef.child("Informative").child(userUID).child(mAuth.getCurrentUser().getUid()).setValue("Placeholder Value");
                        btnInfo.setBackground(getDrawable(R.drawable.informative_red_24dp));
                        userInfo = false;
                    }
                } else if(userProf) {
                    if(dataSnapshot.child("Professionalism").child(userUID).hasChild(mAuth.getCurrentUser().getUid())) {
                        //Remove if already exists
                        userLikesRef.child("Professionalism").child(userUID).child(mAuth.getCurrentUser().getUid()).removeValue();
                        btnProf.setBackground(getDrawable(R.drawable.professionalism_black_24dp));
                        userProf = false;
                    } else {
                        userLikesRef.child("Professionalism").child(userUID).child(mAuth.getCurrentUser().getUid()).setValue("Placeholder Value");
                        btnProf.setBackground(getDrawable(R.drawable.professionalism_red_24dp));
                        userProf = false;
                    }
                }
                //Get count
                int likesCount, dislikesCount, commCount, infoCount, profCount;
                likesCount = (int) dataSnapshot.child("Likes").child(userUID).getChildrenCount();
                dislikesCount = (int) dataSnapshot.child("Dislikes").child(userUID).getChildrenCount();
                commCount = (int) dataSnapshot.child("Communication").child(userUID).getChildrenCount();
                infoCount = (int) dataSnapshot.child("Informative").child(userUID).getChildrenCount();
                profCount = (int) dataSnapshot.child("Professionalism").child(userUID).getChildrenCount();

                lblLikes.setText(Integer.toString(likesCount));
                lblDislikes.setText(Integer.toString(dislikesCount));
                lblComm.setText(Integer.toString(commCount));
                lblInfo.setText(Integer.toString(infoCount));
                lblProf.setText(Integer.toString(profCount));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void updateLikes() {
            userLikesRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(userLiked) {
                        if (dataSnapshot.child("Likes").child(userUID).hasChild(mAuth.getCurrentUser().getUid())) {
                            //Remove like if already likes
                            userLikesRef.child("Likes").child(userUID).child(mAuth.getCurrentUser().getUid()).removeValue();
                            btnLike.setBackground(getDrawable(R.drawable.thumb_up_black_24dp));
                            userLiked = false;

                        } else {
                            //Change placeholder value to a piece of user info
                            //If you want to be able to get a list of users eventually
                            if (dataSnapshot.child("Dislikes").child(userUID).hasChild(mAuth.getCurrentUser().getUid())) {
                                userLikesRef.child("Dislikes").child(userUID).child(mAuth.getCurrentUser().getUid()).removeValue();
                                btnDislike.setBackground(getDrawable(R.drawable.thumb_down_black_24dp));
                            }
                            userLikesRef.child("Likes").child(userUID).child(mAuth.getCurrentUser().getUid()).setValue("Placeholder Value");
                            btnLike.setBackground(getDrawable(R.drawable.thumb_up_red_24dp));
                            userLiked = false;
                        }
                    }
                    else if (userDisliked) {
                        if (dataSnapshot.child("Dislikes").child(userUID).hasChild(mAuth.getCurrentUser().getUid())) {
                            //Remove Dislikes if already Disliked
                            userLikesRef.child("Dislikes").child(userUID).child(mAuth.getCurrentUser().getUid()).removeValue();
                            btnDislike.setBackground(getDrawable(R.drawable.thumb_down_black_24dp));
                            userDisliked = false;
                        } else {
                            //Change placeholder value to a piece of user info
                            //If you want to be able to get a list of users eventually
                            if (dataSnapshot.child("Likes").child(userUID).hasChild(mAuth.getCurrentUser().getUid())) {
                                userLikesRef.child("Likes").child(userUID).child(mAuth.getCurrentUser().getUid()).removeValue();
                                btnLike.setBackground(getDrawable(R.drawable.thumb_up_black_24dp));
                            }
                            userLikesRef.child("Dislikes").child(userUID).child(mAuth.getCurrentUser().getUid()).setValue("Placeholder Value");
                            btnDislike.setBackground(getDrawable(R.drawable.thumb_down_red_24dp));
                            userDisliked = false;
                        }
                    }

                    //Get count
                    int likesCount, dislikesCount;
                    likesCount = (int) dataSnapshot.child("Likes").child(userUID).getChildrenCount();
                    dislikesCount = (int) dataSnapshot.child("Dislikes").child(userUID).getChildrenCount();

                    lblLikes.setText(Integer.toString(likesCount));
                    lblDislikes.setText(Integer.toString(dislikesCount));
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }


    private void openChat() {
        Intent intent = new Intent(ViewOtherProfile.this, MessageActivity.class);
        intent.putExtra("recipientUID", userUID);
        intent.putExtra("senderUID", mAuth.getCurrentUser().getUid());
        finish();
        startActivity(intent);
    }

    private void getData(DataSnapshot dataSnapshot) {
        User user = new User();
        for(DataSnapshot ds : dataSnapshot.getChildren()) {
            Log.d("DataSnapshot", "DataSnapshot: " + ds);
            if(ds.getKey().equals("user")) {
                user.setName(ds.child(userUID).getValue(User.class).getName());
                try {
                    user.setImgURL(ds.child(userUID).getValue(User.class).getImgURL());
                } catch (Exception e) {
                    Log.d("ProfilePic", "User does not have a profil pic");
                }

                user.setName(ds.child(userUID).getValue(User.class).getName());
                user.setBio(ds.child(userUID).getValue(User.class).getBio());
                user.setAge(ds.child(userUID).getValue(User.class).getAge());
                user.setCity(ds.child(userUID).getValue(User.class).getCity());
                user.setEmail(ds.child(userUID).getValue(User.class).getEmail());

            }
        }
        lblName.setText(user.getName());
        lblDOB.setText(user.getAge());
        lblDesc.setText(user.getBio());
        lblCity.setText(user.getCity());
        recipientEmail = user.getEmail();
        Log.d("email", "email: " + recipientEmail);

        try {
            Glide.with(ViewOtherProfile.this)
                    .load(user.getImgURL())
                    .into(profilePic);
        } catch(Exception e) {
            Log.d("ProfilePic", "User does not have a profil pic");
        }
    }
}
