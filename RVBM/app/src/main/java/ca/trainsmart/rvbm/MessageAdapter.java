package ca.trainsmart.rvbm;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {
    public static final int MSG_LEFT = 0;
    public static final int MSG_RIGHT= 1;

    Context context;
    private List<Message> lstMessages;

    FirebaseUser mUser;

    class MessageViewHolder extends RecyclerView.ViewHolder {
        TextView messageBox;

        public MessageViewHolder(View itemView) {
            super(itemView);
            messageBox = (TextView) itemView.findViewById(R.id.txtMessageView);
        }
    }

    public MessageAdapter(Context context, List<Message> lstMessages) {
        this.context = context;
        this.lstMessages = lstMessages;
    }

    @NonNull
    @Override
    public MessageAdapter.MessageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;

        if(i == MSG_RIGHT) {
            view = LayoutInflater.from(context).inflate(R.layout.message_right, viewGroup, false);

        } else {
            view = LayoutInflater.from(context).inflate(R.layout.message_left, viewGroup, false);

        }
        return new MessageAdapter.MessageViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final MessageAdapter.MessageViewHolder messageViewHolder,  int i) {
        String message = lstMessages.get(i).getMessage();
        Log.d("MessageText", message);
        messageViewHolder.messageBox.setText(message);
    }

    @Override
    public int getItemCount() {
        return lstMessages.size();
    }

    @Override
    public int getItemViewType(int position) {
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        if(lstMessages.get(position).getSender().equals(mUser.getUid())) {
            return MSG_RIGHT;
        } else {
            return MSG_LEFT;
        }
    }
}
