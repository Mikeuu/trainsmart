package ca.trainsmart.rvbm;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MessageActivity extends AppCompatActivity {

    TextView name;
    FirebaseUser mUser;
    ImageButton btnSend;
    EditText txtMessage;
    DatabaseReference dbRef;
    Intent intent;
    List<Message> lstMessages;
    RecyclerView messageRecyclerView;
    MessageAdapter msgAdapter;
    String recipientUID;

    @Override
    protected void

    onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        btnSend = (ImageButton) findViewById(R.id.btnSend);
        txtMessage = (EditText) findViewById(R.id.txtMessage);
        messageRecyclerView = (RecyclerView) findViewById(R.id.messageRecyclerView);
        messageRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        messageRecyclerView.setLayoutManager(linearLayoutManager);

        intent = getIntent();
        recipientUID = intent.getStringExtra("recipientUID");
        mUser= FirebaseAuth.getInstance().getCurrentUser();
        dbRef = FirebaseDatabase.getInstance().getReference();
        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                getMessages(mUser.getUid(), recipientUID);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = txtMessage.getText().toString();
                if(!msg.isEmpty()) {
                    sendMessage(mUser.getUid(), recipientUID, msg);
                }
            }
        });

    }

    private void sendMessage(String sender, final String recipient, final String message) {
        dbRef = FirebaseDatabase.getInstance().getReference();
        HashMap<String, Object> senderMap = new HashMap<>();
        senderMap.put("sender", sender);
        senderMap.put("recipient", recipient);
        senderMap.put("message", message);

        dbRef.child("Messages").push().setValue(senderMap);
        final DatabaseReference cRef = FirebaseDatabase.getInstance().getReference("Conversations");
        cRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                    cRef.child(mUser.getUid()).child(recipientUID).child("uid").setValue(recipientUID);
                    cRef.child(recipientUID).child(mUser.getUid()).child("uid").setValue(mUser.getUid());
                    cRef.child(mUser.getUid()).child(recipient).child("message").setValue(message);
                cRef.child(recipientUID).child(mUser.getUid()).child("message").setValue(message);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        txtMessage.setText("");
    }

    private void getMessages(final String currentUID, final String otherUID) {
        lstMessages = new ArrayList<>();
        dbRef = FirebaseDatabase.getInstance().getReference("Messages");
        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lstMessages.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Message msg = snapshot.getValue(Message.class);
                    if(msg.getRecipient().equals(currentUID) && msg.getSender().equals(otherUID) || msg.getRecipient().equals(otherUID) && msg.getSender().equals(currentUID)) {
                        lstMessages.add(msg);
                    }

                    msgAdapter = new MessageAdapter(MessageActivity.this, lstMessages);
                    messageRecyclerView.setAdapter(msgAdapter);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
