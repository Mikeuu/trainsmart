package ca.trainsmart.rvbm;

import java.util.Date;

public class User {

    String name, email, uid, bio, accountType, city, age, imgURL, profession, gender;

    public User() {

    }

    public User(String uid) {
        this.uid = uid;
    }

    public User(String uid, String email, String name, String bio, String accountType, String age, String city,
                 String profession, String imgURL, String gender) {
        this.uid = uid;
        this.name = name;
        this.email = email;
        this.bio = bio;
        this.accountType = accountType;
        this.city = city;
        this.imgURL = imgURL;
        this.profession = profession;
        this.age = age;
        this.gender = gender;

    }

    //Professional
    public User(String uid, String email, String name, String bio, String accountType, String age, String city,
                String profession, String gender) {
        //(email, name, aboutMe, accountType, bday, city, profession);
        this.uid = uid;
        this.email = email;
        this.name = name;
        this.bio = bio;
        this.accountType = accountType;
        this.age = age;
        this.city = city;
        this.profession = profession;
        this.gender = gender;

    }

    //Base account information to register
    public User(String email, String uid) {
        this.email = email;
        this.uid = uid;
    }

    //Extra info
    public User(String uid, String email, String name, String bio, String accountType, String age, String city, String gender) {
        this.uid = uid;
        this.email = email;
        this.name = name;
        this.bio = bio;
        this.accountType = accountType;
        this.age = age;
        this.city = city;
        this.gender = gender;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public void setImgURL(String url) {
        this.imgURL = url;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getUid() {
        return uid;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
}

    public void setGender(String gender) {
        this.gender = gender;
    }
}
