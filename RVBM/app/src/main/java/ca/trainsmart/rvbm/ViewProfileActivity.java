package ca.trainsmart.rvbm;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ViewProfileActivity extends AppCompatActivity {

    private static final String TAG = "State";

    private ImageView profilePic;
    private TextView lblName, lblDesc, lblDOB, lblCity, lblProfession;
    private ImageButton btnLogout, btnEdit, btnHome;

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private DatabaseReference userInfoRef;
    private String currentUID;
    private boolean accountPlus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);

        profilePic = (ImageView) findViewById(R.id.ivProfilePic);
        lblName = (TextView) findViewById(R.id.lblName);
        lblDesc = (TextView) findViewById(R.id.lblDescription);
        lblDOB = (TextView) findViewById(R.id.lbDateOfBirth);
        lblCity = (TextView) findViewById(R.id.lbCity);
        lblProfession = (TextView) findViewById(R.id.lblProfession);
        btnLogout = (ImageButton) findViewById(R.id.btnLogout);
        btnEdit = (ImageButton) findViewById(R.id.btnEdit);
        btnHome = (ImageButton) findViewById(R.id.btnHome);


        mAuth  = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        currentUID = mUser.getUid();
        userInfoRef = FirebaseDatabase.getInstance().getReference();

        userInfoRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                getData(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //Logout button
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                finish();
                startActivity(new Intent(ViewProfileActivity.this, LoginActivity.class));
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(ViewProfileActivity.this, ProfileActivity.class));
            }
        });

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ViewProfileActivity.this, HomeActivity.class));
            }
        });

    }

    private void getData(DataSnapshot dataSnapshot) {
        User user = new User();
        for(DataSnapshot ds : dataSnapshot.getChildren()) {
            Log.d("DataSnapshot", "DataSnapshot: " + ds);
            if(ds.getKey().equals("user")) {
                user.setName(ds.child(currentUID).getValue(User.class).getName());
                try {
                    user.setImgURL(ds.child(currentUID).getValue(User.class).getImgURL());
                } catch (Exception e) {
                    Log.d("ProfilePic", "User does not have a profil pic");
                }
                user.setName(ds.child(currentUID).getValue(User.class).getName());
                user.setBio(ds.child(currentUID).getValue(User.class).getBio());
                user.setAge(ds.child(currentUID).getValue(User.class).getAge());
                user.setCity(ds.child(currentUID).getValue(User.class).getCity());
                user.setAccountType(ds.child(currentUID).getValue(User.class).getAccountType());
                accountPlus = user.getAccountType().equals("Health Professional");
                if (accountPlus) {
                    user.setProfession(ds.child(currentUID).getValue(User.class).getProfession());
                    Log.i(TAG, user.getProfession());
                }

            }
        }
         lblName.setText(user.getName());
        lblDOB.setText(user.getAge());
        lblDesc.setText(user.getBio());
        lblCity.setText(user.getCity());
        if (accountPlus) {
            lblProfession.setVisibility(View.VISIBLE);
            lblProfession.setText(user.getProfession());
        }

        try {
            Glide.with(ViewProfileActivity.this)
                    .load(user.getImgURL())
                    .into(profilePic);
        } catch(Exception e) {
            Log.d("ProfilePic", "User does not have a profil pic");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(mAuth.getCurrentUser() == null) {
            finish();
            startActivity(new Intent(ViewProfileActivity.this, LoginActivity.class));
         }
    }
}
