package ca.trainsmart.rvbm;

public class ConvoList {
    String uid;

    public ConvoList() {

    }

    public ConvoList(String uid) {
        this.uid = uid;
    }

    public String getuid() {
        return uid;
    }

    public void setuid(String uid) {
        this.uid = uid;
    }
}
